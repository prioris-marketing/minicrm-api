<?php 
/**
 * MiniCRM API: PHP Client library for R3 API (Template)
 *
 * Makes accessing MiniCRM API easy from PHP apps.
 * Address data management
 *
 * @package MiniCRM-API-Client
 * @author Csaba Keszei
 * @version 3.3
 * @copyright Copyright (C) 2009 - 2013 MiniCRM Zrt. All rights reserved.
 * @license GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 */
 
namespace PriorisMarketing\MiniCRM_API;

use Exception;
use PriorisMarketing\MiniCRM_API\MiniCRM_BaseDao;

class MiniCRM_Template extends MiniCRM_BaseDao {	

	public function __construct(MiniCRM_Connection $Connection, $Id = 0) {
	 	parent::__construct($Connection, 'Template', (int) $Id);
	}


	/**
	 * @throws Exception
	 * @deprecated This method is not supported in Template model!
	*/
	public function __set($Field, $Value) {
		$this->log->critical('This method is not supported! __set() MiniCRM_Template');
	}
	
	
	/**
	 * @throws Exception
	 * @deprecated This method is not supported in Template model!
	*/
	public function Save(bool $priority = false): int {
		$this->log->critical('This method is not supported! Save() MiniCRM_Template');
		return 0;
	}


	public static function TemplateList(MiniCRM_Connection $Connection, int $CategoryId, $CreatedAt = false) {
		return $Connection->Request("TemplateList/{$CategoryId}".($CreatedAt ? "?CreatedAt=".$CreatedAt : ""), false, 'GET');
	}
}