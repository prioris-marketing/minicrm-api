<?php

/**
 * MiniCRM API: PHP Client library for R3 API (Contact)
 *
 * Makes accessing MiniCRM API easy from PHP apps.
 * Contact data management
 *
 * @package MiniCRM-API-Client
 * @author Csaba Keszei
 * @version 3.3
 * @copyright Copyright (C) 2009 - 2013 MiniCRM Zrt. All rights reserved.
 * @license GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace PriorisMarketing\MiniCRM_API;

use Exception;
use PriorisMarketing\MiniCRM_API\MiniCRM_BaseDao;

class MiniCRM_Contact extends MiniCRM_BaseDao
{
	private $Type;
	private $Tags = [];


	public function __construct(MiniCRM_Connection $Connection, $Id = 0, $Type = false, $loadTags = false)
	{
		if (!$Id && !$Type) throw new Exception('Id or Type required!');
		if (!$Id) $this->Type = $Type;

		parent::__construct($Connection, 'Contact', (int) $Id);

		if (!$loadTags) {
			$this->Values['Tags'] = [];
		}
	}


	protected function Load()
	{
		if (!$this->Id) return;

		parent::Load();

		switch ($this->Values['Type']) {
			case "Cég" || "Business":
				$this->Type = 'Business';
				break;
			case "Személy" || "Person":
				$this->Type = 'Person';
				break;
			default:
				throw new Exception('Invalid Type!');
		}
	}

	public function AddTag($TagName)
	{
		$this->Tags[] = $TagName;
	}


	public function Save(bool $priority = false): int 
	{
		if ($this->Id) {
			unset($this->Changed['Type']);
		} else {
			$this->Values['Type'] = $this->Type;
			$this->Changed['Type'] = 1;
		}

		if (@$this->Changed['EmailType']) $this->Changed['Email'] = 1;
		if (@$this->Changed['PhoneType']) $this->Changed['Phone'] = 1;

		if (count($this->Tags) > 0) {
			$this->Values['Tags'] = $this->Tags;
			$this->Changed['Tags'] = 1;
		}

		$Id = parent::Save($priority);

		return $Id;
	}


	public static function GetSchema(MiniCRM_Connection $Connection, string $Type = 'Business')
	{
		return $Connection->Request("Schema/{$Type}/", false, 'GET');
	}


	public static function Search(MiniCRM_Connection $Connection, string $QueryString)
	{
		return $Connection->Request('Contact', ['Query' => $QueryString], 'GET');
	}


	public static function FieldSearch(MiniCRM_Connection $Connection, $Fields)
	{
		if (!is_array($Fields)) throw new Exception("Search Fields must be an array, " . gettype($Fields) . " is given!");
		return $Connection->Request('Contact', $Fields, 'GET');
	}
}
