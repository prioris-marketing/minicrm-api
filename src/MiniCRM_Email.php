<?php 
/**
 * MiniCRM API: PHP Client library for R3 API (Email)
 *
 * Makes accessing MiniCRM API easy from PHP apps.
 * Address data management
 *
 * @package MiniCRM-API-Client
 * @author Csaba Keszei
 * @version 3.3
 * @copyright Copyright (C) 2009 - 2013 MiniCRM Zrt. All rights reserved.
 * @license GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 */
 
namespace PriorisMarketing\MiniCRM_API;

use PriorisMarketing\MiniCRM_API\MiniCRM_BaseDao;

class MiniCRM_Email {	

	public static function EmailList(MiniCRM_Connection $Connection, int|bool $ProjectId = false, $CreatedAt = false) {
		$Url = "EmailList/".$ProjectId ?: '';

		if($CreatedAt) $Url .= "?CreatedAt={$CreatedAt}";

		return $Connection->Request($Url, false, 'GET');
	}
}