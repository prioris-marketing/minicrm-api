<?php

/**
 * MiniCRM API: PHP Client library for R3 API (Connection)
 *
 * Makes accessing MiniCRM API easy from PHP apps.
 * Connection management part of the script
 *
 * @package MiniCRM-API-Client
 * @author Csaba Keszei
 * @version 3.3
 * @copyright Copyright (C) 2009 - 2013 MiniCRM Zrt. All rights reserved.
 * @license GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace PriorisMarketing\MiniCRM_API;

use Exception;

class MiniCRM_Connection
{
    private string $ApiUrl;
    private $isDirectConnection;
    private LogWrapper $log;
    const MaxRetries = 3;

    public function __construct(int $SystemId, string $ApiKey, array|false $Proxy = false)
    {
        $this->log = new LogWrapper();

        if (!in_array('curl', get_loaded_extensions())) {
            $this->log->critical('Curl needed to run this script!');
        }

        if (strlen($ApiKey) != 32) {
            $this->log->critical('Invalid API Key!');
        }

        $this->isDirectConnection = $Proxy['isDirectConnection'] ?? true;

        if ($Proxy) {
            $Domain = $Proxy['Domain'];
            $Schema = $Proxy['Schema'];
        } else {
            $Domain = "r3.minicrm.hu";
            $Schema = "https";
        }

        $this->ApiUrl = "{$Schema}://{$SystemId}:{$ApiKey}@{$Domain}/Api/R3/";
    }

    public function Request(string $Uri, array|false $Params, string $Method, bool $priority = false): array
    {
        $Curl = curl_init();;

        curl_setopt($Curl, CURLOPT_USERAGENT, 'MiniCRM_API/3.0');
        curl_setopt($Curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($Curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($Curl, CURLOPT_TIMEOUT, 600);
        curl_setopt($Curl, CURLOPT_CONNECTTIMEOUT, 300);

        if ($Method == 'PUT') {
            $Url = $this->ApiUrl . $Uri;

            if ($priority && !$this->isDirectConnection) {
                $Params["PriorityProxyPass"] = true;
            }

            $ParamsJSON = json_encode($Params);
            if ($ParamsJSON === false) {
                $this->log->critical('Parameters JSON encoding failed!');
            }

            curl_setopt($Curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Content-Length: ' . strlen($ParamsJSON), 'charset=UTF-8']);
            curl_setopt($Curl, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($Curl, CURLOPT_POSTFIELDS, $ParamsJSON);
        } else {
            $Url = $this->ApiUrl . $Uri . ($Params ? '?' . http_build_query($Params) : '');
        }

        curl_setopt($Curl, CURLOPT_URL, $Url);

        $Response = curl_exec($Curl);
        $this->log->info("$Method: $Url", ['Response' => $Response, 'Request' => $Params]);

        if (curl_errno($Curl))
            $Error = "Curl Error: " . curl_error($Curl);

        $ResponseCode = curl_getinfo($Curl, CURLINFO_HTTP_CODE);

        if ($ResponseCode != 200) {
            if ($ResponseCode === 401) {
                $this->log->critical("API Error - Code: {$ResponseCode} - Message: Can't connect with the MiniCRM servers (Maybe bad API key or systemD)!");
            }
            $this->log->critical("API Error - Code: {$ResponseCode} - Message: {$Response}");
        }

        curl_close($Curl);

        $BOM = pack('CCC', 239, 187, 191);
        while (0 === strpos($Response, $BOM)) {
            $Response = substr($Response, 3);
        }

        $Response = json_decode($Response, true);

        return $Response;
    }
}
