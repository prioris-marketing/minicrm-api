<?php
namespace PriorisMarketing\MiniCRM_API;

use Illuminate\Support\ServiceProvider;

class MiniCRMServiceProvider extends ServiceProvider {

    public function boot()
    {
        // Publish the config file to the Laravel application
        $this->publishes([
            __DIR__ . '/../config/minicrmlog.php' => config_path('minicrmlog.php'),
        ], 'minicrm-log-config');

        // Ensure the log directory exists
        $this->ensureLogDirectoryExists();
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/minicrmlog.php', 'minicrmlog');
    }

    protected function ensureLogDirectoryExists()
    {
        $logPath = config('minicrmlog.daily.path', storage_path('logs/mini-crm/crm.log'));

        if (!is_dir(dirname($logPath))) {
            mkdir(dirname($logPath), 0755, true);
        }
    }
}