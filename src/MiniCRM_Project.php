<?php
/**
 * MiniCRM API: PHP Client library for R3 API (Project)
 *
 * Makes accessing MiniCRM API easy from PHP apps.
 * Project data management
 *
 * @package MiniCRM-API-Client
 * @author Csaba Keszei
 * @version 3.3
 * @copyright Copyright (C) 2009 - 2013 MiniCRM Zrt. All rights reserved.
 * @license GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace PriorisMarketing\MiniCRM_API;

use Exception;
use PriorisMarketing\MiniCRM_API\MiniCRM_BaseDao;

class MiniCRM_Project extends MiniCRM_BaseDao {
	private $CategoryId;

	public function __construct(MiniCRM_Connection $Connection, int $Id = 0, int $CategoryId = 0) {
		if ($Id <= 0 && $CategoryId <= 0) {
			$this->log->critical('Id or categoryId is required!', ['Id' => $Id, 'CategoryId' => $CategoryId]);
		}
		
		if($Id === 0) $this->CategoryId = $CategoryId;

	 	parent::__construct( $Connection, 'Project', (int) $Id);
	}


	protected function Load(): void {
		if (!$this->Id) return;

		parent::Load();

		$this->CategoryId = $this->Values['CategoryId'];
	}


	public function Save(bool $priority = false): int {
		if ($this->Id) {
			unset($this->Changed['CategoryId']);
		} else {
			$this->Values['CategoryId'] = $this->CategoryId;
			$this->Changed['CategoryId'] = 1;
		}
		$Id = parent::Save($priority);

		return $Id;
	}


	public static function GetSchema(MiniCRM_Connection $Connection, int $CategoryId = 0) {
		return $Connection->Request("Schema/Project/{$CategoryId}", false, 'GET');
	}


	public static function Search(MiniCRM_Connection $Connection, string $QueryString) {
		return $Connection->Request('Project', ['Query' => $QueryString], 'GET');
	}


	public static function FieldSearch(MiniCRM_Connection $Connection, array $Fields) {
		return $Connection->Request('Project', $Fields, 'GET');
	}


	public static function GetCategories(MiniCRM_Connection $Connection, bool $Detailed = false) {
		$Url = $Detailed ? "Category?Detailed=1" : "Category";
		return $Connection->Request($Url, false, 'GET');
	}
}
