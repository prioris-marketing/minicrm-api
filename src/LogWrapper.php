<?php

namespace PriorisMarketing\MiniCRM_API;

use Illuminate\Support\Facades\Log as LaravelLog;
use JsonSerializable;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LogWrapper implements JsonSerializable
{
    protected $logger;

    public function __construct()
    {
        if (function_exists('app') && app()->bound('log')) {
            $channel = config('minicrmlog.default_log_channel', 'daily');
            $this->logger = LaravelLog::channel($channel);
        } else {
            $this->logger = new Logger('MiniCRM_API_logger');
            $this->logger->pushHandler(new StreamHandler('logs/test.log', Logger::DEBUG));
        }
    }

    public function info(string $message, array $context = []): void
    {
        $message = "[MiniCRM_API - info] $message";
        $this->logger->info($message, $context);
    }

    public function error(string $message, array $context = []): void
    {
        $message = "[MiniCRM_API - error] $message";
        $this->logger->error($message, $context);
    }

    public function debug(string $message, array $context = []): void
    {
        $message = "[MiniCRM_API - debug] $message";
        $this->logger->debug($message, $context);
    }

    public function critical(string $message, array $context = []): void
    {
        $message = "[MiniCRM_API - critical] $message";
        $this->logger->critical($message, $context);
        throw new \Exception($message);
    }

    public function __sleep(): array 
    {
        return [];
    }

    public function __wakeup()
    {
        // Restore the logger after unserialization
        if (function_exists('app') && app()->bound('log')) {
            $this->logger = LaravelLog::channel(config('minicrmlog.default_log_channel', 'daily'));
        } else {
            $this->logger = new Logger('MiniCRM_API_logger');
            $this->logger->pushHandler(new StreamHandler('logs/test.log', Logger::DEBUG));
        }
    }

    public function jsonSerialize(): array
    {
        return [];
    }
}
