<?php
/**
 * MiniCRM API: PHP Client library for R3 API (ToDo)
 *
 * Makes accessing MiniCRM API easy from PHP apps.
 * ToDo data management
 *
 * @package MiniCRM-API-Client
 * @author Csaba Keszei
 * @version 3.3
 * @copyright Copyright (C) 2009 - 2013 MiniCRM Zrt. All rights reserved.
 * @license GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace PriorisMarketing\MiniCRM_API;

use PriorisMarketing\MiniCRM_API\MiniCRM_BaseDao;

class MiniCRM_ToDo extends MiniCRM_BaseDao {

	public function __construct(MiniCRM_Connection $Connection, $Id = 0) {
	 	parent::__construct($Connection, 'ToDo', (int) $Id);
	}


	public function Save(bool $priority = false): int {
		if ($this->Id) {
			unset($this->Changed['ProjectId']);
		}
		$Id = parent::Save($priority);

		return $Id;
	}

	public static function ToDoList(MiniCRM_Connection $Connection, int $ProjectId, $Type = 'All') {
		return $Connection->Request("ToDoList/{$ProjectId}", ['Status' => $Type], 'GET');
	}
}
