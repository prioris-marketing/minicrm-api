<?php

/**
 * MiniCRM API: PHP Client library for R3 API (BaseDao)
 *
 * Makes accessing MiniCRM API easy from PHP apps.
 * Basic data management part of the script
 *
 * @package MiniCRM-API-Client
 * @author Csaba Keszei
 * @version 3.3
 * @copyright Copyright (C) 2009 - 2013 MiniCRM Zrt. All rights reserved.
 * @license GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace PriorisMarketing\MiniCRM_API;

use Exception;
use JsonSerializable;

abstract class MiniCRM_BaseDao implements JsonSerializable {
	protected int $Id = 0;
	protected $Table;
	protected $Connection;
	protected array $Values = [];
	protected array $Changed = [];
	protected $IsLoaded = false;
	protected LogWrapper $log;

	public function __construct(MiniCRM_Connection $Connection, string $Table, int $Id = 0) {
		$this->log = new LogWrapper();
		
		if ($Id <= 0 ) {
			$this->log->info('Missing ID maybe is it a new record?', ['ID' => $Id]);
		}

		$this->Id = $Id;
		$this->Connection = $Connection;
		$this->Table = $Table;
	}


	public function __get($Field) {
		$this->CheckLoad();
	
		return $this->Values[$Field];
	}


	public function __set($Field, $Value) {
		if (!array_key_exists($Field, $this->Values) || ($this->Values[$Field] != $Value)) {
			$this->Changed[$Field] = 1;
			$this->Values[$Field] = $Value;
		}
	}

	public function __isset($Field) {
		$this->CheckLoad();

		return isset($this->Values[$Field]);
	}

	public function __unset($Field) {
		unset($this->Values[$Field]);
	}

	public function jsonSerialize(): mixed {
		return $this->GetValues();
	}

	protected function Load() {
		if (!$this->Id) return;

		$this->Values = array_merge($this->Connection->Request("{$this->Table}/{$this->Id}", false, 'GET'), $this->Values);
		$this->IsLoaded = true;
	}

	protected function CheckLoad()
	{
		if(!$this->IsLoaded){
			$this->Load();
		}
	}

	public function Save(bool $priority = false): int {
		$Fields = [];
		foreach ($this->Changed as $Field => $Changed) {
			$Value = is_array($this->Values[$Field]) ? $this->Values[$Field] : trim($this->Values[$Field]);
			$Fields[$Field] = $Value;
		}

		if (count($Fields) < 1) return $this->Id;

		$Fields['Id'] = $this->Id;

		$Url = $this->Id ? "{$this->Table}/{$this->Id}" : "{$this->Table}/";

		$Response = $this->Connection->Request($Url, $Fields, 'PUT',  $priority);

		$this->Changed = [];
		$this->Values['Id'] = $Response['Id'];

		return $Response['Id'];
	}

	public function GetValues(){
		$this->CheckLoad();
		
		return $this->Values;
	}
}
