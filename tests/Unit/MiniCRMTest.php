<?php

use PriorisMarketing\MiniCRM_API\LogWrapper;
use PriorisMarketing\MiniCRM_API\MiniCRM_Connection;
use PriorisMarketing\MiniCRM_API\MiniCRM_Project;


$miniCrmConnection = null;
$crmCreds = null;
$project = null;
$testLogPath = __DIR__ . '/../../logs/test.log';

test('Standalone Logging', function () use ($testLogPath)  {    
    $log = new LogWrapper();
    $log->info('Logging test');

    expect(getLastLineOfLog($testLogPath))->toContain('Logging test');
});

test('MiniCRM Connection Class', function () use (&$miniCrmConnection, &$crmCreds) {
    $crmCreds = loadCrmCredentials();
    $miniCrmConnection = new MiniCRM_Connection($crmCreds['systemId'], $crmCreds['apiKey'], $crmCreds);

    expect($miniCrmConnection)->toBeInstanceOf(MiniCRM_Connection::class);
});

test('MiniCRM Connection', function () use (&$miniCrmConnection) {
    $crmProject = MiniCRM_Project::GetCategories($miniCrmConnection);
    expect($crmProject)->toBeArray();
});

test('MiniCRM Get project', function () use (&$miniCrmConnection, &$crmCreds, &$project) {
    $project = new MiniCRM_Project($miniCrmConnection, $crmCreds['projectId'], $crmCreds['categoryId']);
    expect($project->Id)->toBeInt();
});

test('MiniCRM Modify a project', function () use (&$project, &$crmCreds, $testLogPath) {
    $var = $crmCreds['projectTestVar'];
    $rndStr = str()->random(10);
    $project->$var = $rndStr;
    $project->Save();
    expect(getLastLineOfLog($testLogPath))->toContain($rndStr);
});

function loadCrmCredentials(): array 
{
    $credentialsAsArray = json_decode(file_get_contents(__DIR__ . '/crm_credentials.json'), true);
    return $credentialsAsArray;
}

function getLastLineOfLog(string $logPath): string {
    $file = file($logPath);
    $lastLine = end($file);
    return $lastLine;
}