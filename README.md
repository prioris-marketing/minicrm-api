# MiniCRM API

[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html)

Composer Compatible MiniCRM PHP API for R3
Based on [MiniCRM API php Client (R3, uncompressed)](https://bitbucket.org/minicrm/api-php-client-r3-uncompressed/)

# ChangeLog

## [1.5.0] - 2019-04-16

### Feature
- ```__isset``` and ``` __unset``` function for BaseDao

### Fix
- psr4 compability

## [1.4.0] - 2019-04-16

### Fix
- BaseDao Save function. Fields variable was not defined.

## [1.3.0] - 2019-04-16

New function to return Model's Values array. The motivation is an esier use of Laravel Validator.

### Added
- GetValues function

## [1.2.0] - 2019-04-03

New parameter for Contact model constructor.

In the original code, the tags was been removed from Contacts. But sometimes you need this tags, so there is a switch for that to load or remove the tags. The default value for this is to remove tags, but if you give a 4th true param the tags will be not deleted.

```php
$contact_with_tags = new MiniCRM_Contact($connection, 12345, false, true);
```

### Added 
- loadTag parameter to MC_Contact constructor.


## [1.1.0] - 2019-03-20

Composer compability

### Added
- Namespaces for PHP files
- License file added with GNU/GPLv3
- composer.json and composer.lock
- .gitignore file for ignoring vendor folder

### Changed
- Moved source files to src folder

## [1.0.0] Original
Original state from "fork". Based on [MiniCRM API php Client (R3, uncompressed) [e0beb0f]](https://bitbucket.org/minicrm/api-php-client-r3-uncompressed/commits/e0beb0fc60422e8f013a0bfe993730621eb93662)
