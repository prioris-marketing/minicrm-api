<?php

return [
    'default_log_channel' => 'minicrmdaily',
    'path' => storage_path('logs/mini-crm/crm.log'),
    'level' => 'debug',
    'days' => 30,
];